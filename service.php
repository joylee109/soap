<?php
ini_set("soap.wsdl_cache_enabled","0");
define('WSDL_FILE', 'soapHandle.wsdl');

if (! file_exists(WSDL_FILE)) {

    require_once('SoapDiscovery.class.php');

    $disco = new SoapDiscovery('soapHandle', 'test');

    $disco->getWSDL();
}

$server = new SoapServer(WSDL_FILE);

//注册Service类的所有方法
$server->setClass("soapHandle");

//处理请求
$server->handle();

class soapHandle
{
    public function strToLink($url = '')
    {
        return sprintf('<a href="%s">%s</a>', $url, $url);
    }

    public function add($a = 1, $b = 2)
    {
        return $a + $b;
    }

    public function findSomeOne($name)
    {
        return '你好，ABC' . $name;
    }

    public function destroy()
    {
        return 'destroy';
    }
}
